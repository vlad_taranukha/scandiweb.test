jQuery(function ($) {

    /*Collects products to delete*/
    $('#delete-product-btn').click(function (e) {
        let delChecks = $('.delete-checkbox:checked');
        if (delChecks.length == 0) {
            e.preventDefault();
        }else {
            let productsToDelete = [];
            delChecks.each(function (i, el) {
                productsToDelete.push("'"+$(el).attr('value')+"'");
            });
            $('#delete-product-btn').attr('value', productsToDelete.join(', '));
        }
    });

    /*Show/Hide switch characteristics*/
    $('#productType').change(function () {
        let switchCharacteristics = $('.switch-characteristics');
        let charsToShow = '#'+$(this).val();
        $('#type-switcher').attr('disabled', 'disabled');
        $(switchCharacteristics).children().css('display', 'none');
        $(switchCharacteristics).find('input').removeAttr('required');
        $(charsToShow).css('display', 'block');
        $(charsToShow).find('input').attr('required', 'required');
    });

    /*SKU validation*/
    $('#sku').keyup(function(){
        let skuVal = $(this).val().toLowerCase().trim();

        $.ajax({
            url: 'index.php', // путь к обработчику берем из атрибута action
            type: 'POST', // метод передачи - берем из атрибута method
            data: {skuQuery: true},
            dataType: 'json',
            success: function(json){
                let sku =  $('#sku');
                let skuArr = json;
                if (skuVal == '') {
                    $(sku).css('border-color', 'red');
                    $('.sku_exists').css('display', 'none');
                    $(sku).parent('div').addClass('charactShowErr').removeClass('charactHideErr');
                    return false;
                }
                if (skuArr.includes(skuVal)) {
                    $(sku).parent('div').addClass('charactShowErr').removeClass('charactHideErr');
                    $(sku).css('border-color', 'red');
                    $('.sku_exists').text('SKU "'+skuVal+'" already exists, please try another one');
                    $('.sku_exists').css('display', 'inline-block');
                    return false;
                }else {
                    $(sku).parent('div').addClass('charactHideErr').removeClass('charactShowErr');
                    $(sku).css('border-color', '#0fca00');
                    $('.sku_exists').css('display', 'none');
                    return true;
                }
            }
        });
    });

    /*Name validation*/
    $('#name').keyup(function () {
        if ($(this).val() == '') {
            $(this).parent('div').addClass('charactShowErr').removeClass('charactHideErr');
            $(this).css('border-color', 'red');
        }else {
            $(this).css('border-color', '#0fca00');
            $(this).parent('div').addClass('charactHideErr').removeClass('charactShowErr');
        }
    });

    /*Input validation*/
    function validateInput(inputEl, re) {

        inputEl.on('change keyup', function () {
            let inputVal = $(this).val();
            let res = re.test(inputVal);
            if (!res) {
                if ($(this).attr('id') == 'price') {
                    $('.price_example').css('display', 'block');
                }
                $(this).css('border-color', 'red');
                $(this).parent('div').addClass('charactShowErr').removeClass('charactHideErr');
                return false;
            } else {
                if ($(this).attr('id') == 'price') {
                    $('.price_example').css('display', 'none');
                }
                $(this).parent('div').addClass('charactHideErr').removeClass('charactShowErr');
                $(this).css('border-color', '#0fca00');
                return true;
            }
        });
    }

    /*Validate inputs via regexp*/
    validateInput($('#price'), /^((0{1}\.{1}\d{2})|([1-9]+\.{1}\d{2})|([1-9]+\d*))$/m);
    validateInput($('#size'), /^[1-9][0-9]*$/m);
    validateInput($('#weight'), /^[1-9][0-9]*$/m);
    validateInput($('#length'), /^[1-9][0-9]*$/m);
    validateInput($('#width'), /^[1-9][0-9]*$/m);
    validateInput($('#height'), /^[1-9][0-9]*$/m);

    /*Checking inputs before submitting*/
    /*Checking for invalid validation of a separate element function*/
    function checkErr(el) {
        let elParent = $(el).parent('div');
        if (elParent.hasClass('charactShowErr')) {
            return true;
        } else {
            return false;
        }
    }

    /*Checking of invalid validation of select element*/
    function checkSelect (selEl) {
        let selectVal = $(selEl).val();
        if (selectVal == 'DVD') {
            let res = checkErr('#size');
            return res;
        }else if (selectVal == 'Book') {
            let res = checkErr('#weight');
            return res;
        } else if (selectVal == 'Furniture') {
            let resArr = [];
            resArr.push(checkErr('#height'));
            resArr.push(checkErr('#width'));
            resArr.push(checkErr('#length'));
            let res = resArr.includes(true);
            return res;
        }
    }

    /*Checking of invalid validation of the form*/
    $('#add-product-btn').click(function (e) {
        if (checkErr('#sku') || checkErr('#name') || checkErr('#price') || checkSelect('#productType')) {
            e.preventDefault();
            return;
        }
    });

});