<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="style.css">
    <script src="js/jquery.min.js"></script>
    <script src="js/main.js"></script>
    <title>Add product</title>
</head>
<body>
<header>
    <h1>product Add</h1>
    <div class="header-buttons">
        <button form="product_form" type="submit" id="add-product-btn">Save</button>
        <form action="/" method="POST">
            <button class="cancelBtn" onclick="location.href='/'">Cancel</button>
        </form>
    </div>
</header>
<main>
    <form id="product_form" action="/" method="post">
        <div class="main-characteristics">
            <div class="charact">
                <label for="sku">SKU</label>
                <input
                        type="text"
                        id="sku"
                        name="sku"
                        maxlength="100"
                        required
                        oninvalid="this.setCustomValidity('Please, submit required data')"
                        oninput="setCustomValidity('')"
                >
                <p class="sku_exists"></p>
            </div>
            <div class="charact">
                <label for="name">Name</label>
                <input
                        type="text"
                        id="name"
                        name="name"
                        maxlength="100"
                        required
                        oninvalid="this.setCustomValidity('Please, submit required data')"
                        oninput="setCustomValidity('')"
                >
            </div>
            <div class="charact">
                <label for="price">Price ($)</label>
                <input
                        type="number"
                        min="0"
                        step="0.01"
                        id="price"
                        name="price"
                        required
                        oninvalid="this.setCustomValidity('Please, submit required data')"
                        oninput="setCustomValidity('')"
                >
                <p class="price_example">Price should be integer (1, 10 etc.) of decimal with scale "2" (0.99, 10,49 etc.)</p>
                <div class="break"></div>
            </div>
        </div>
        <div class="type-switcher">
            <p>Type Switcher</p>
            <select
                    name="productType"
                    id="productType"
                    required
                    oninvalid="this.setCustomValidity('Please, submit required data')"
                    oninput="setCustomValidity('')">
                <option id="type-switcher" value="">Type Switcher</option>
                <option value="DVD">DVD</option>
                <option value="Book">Book</option>
                <option value="Furniture" >Furniture</option>
            </select>
        </div>
        <div class="switch-characteristics">
            <div id="DVD">
                <div class="size">
                    <label for="size">Size (MB)</label>
                    <input
                            id="size"
                            name="size"
                            type="number"
                            min="0"
                            step="1"
                            oninvalid="this.setCustomValidity('Please, submit required data')"
                            oninput="setCustomValidity('')"
                    >
                </div>
                <p class="charact-descr">Please enter the size of DVD in MB</p>
            </div>
            <div id="Furniture">
                <div class="height">
                    <label for="height">Height (CM)</label>
                    <input
                            id="height"
                            name="height"
                            type="number"
                            min="0"
                            step="1"
                            oninvalid="this.setCustomValidity('Please, submit required data')"
                            oninput="setCustomValidity('')">
                </div>
                <div class="width">
                    <label for="width">Width (CM)</label>
                    <input
                            id="width"
                            name="width"
                            type="number"
                            min="0"
                            step="1"
                            oninvalid="this.setCustomValidity('Please, submit required data')"
                            oninput="setCustomValidity('')">
                </div>
                <div class="length">
                    <label for="length">Length (CM)</label>
                    <input
                            id="length"
                            name="length"
                            type="number"
                            min="0"
                            step="1"
                            oninvalid="this.setCustomValidity('Please, submit required data')"
                            oninput="setCustomValidity('')">
                </div>
                <p class="charact-descr">Please provide dimensions in HxWxL format</p>
            </div>
            <div id="Book">
                <div class="weight">
                    <label for="weight">Weight (KG)</label>
                    <input
                            id="weight"
                            name="weight"
                            type="number"
                            oninvalid="this.setCustomValidity('Please, submit required data')"
                            oninput="setCustomValidity('')">
                </div>
                <p class="charact-descr">Please provide total weight of the book in KG</p>
            </div>
        </div>
    </form>
</main>
<footer>
    <p>Scandiweb Test assignment</p>
</footer>
</body>
</html>