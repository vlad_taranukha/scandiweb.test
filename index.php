<?php
/*App constants (connection to DB)*/
require_once ('App/constants.php');
/*Abstract class App*/
require_once ('App/App.php');
/*Product class*/
require_once ('App/Product/Product.php');
/*DVD class*/
require_once ('App/Product/DVD/DVD.php');
/*Book class*/
require_once ('App/Product/Book/Book.php');
/*Furniture class*/
require_once ('App/Product/Furniture/Furniture.php');

/*Deleting products*/
if (isset($_POST['productsToDelete'])) {
    Product::deleteProducts();
}

/*Adding products*/
if (isset($_POST['sku'])) {
    Product::addProduct();
}

/*Getting skuList for sku input validation*/
if (isset($_POST['skuQuery'])) {
    echo Product::getSkuList();
    exit;
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="style.css">
    <script src="js/jquery.min.js"></script>
    <script src="js/main.js"></script>
    <title>Product List</title>
</head>
<body>
<header>
    <h1>product list</h1>
    <div class="header-buttons">
        <button onclick="location.href='add-product.php'" id="add-product-btn">ADD</button>
        <form method="post">
            <button type="submit" id="delete-product-btn" name="productsToDelete">MASS DELETE</button>
        </form>
    </div>
</header>
<main class="product-list-wrapper">
    <div class="product-list-inner">
        <!--Display product list-->
        <?php Product::showProducts();?>
    </div>
</main>
<footer>
    <p>Scandiweb Test assignment</p>
</footer>
</body>
</html>