<?php

abstract class App {
    abstract public static function dbConnect();
    abstract public static function getSkuList();
    abstract public static function addProduct();
    abstract public static function deleteProducts();
    abstract public static function showProducts();
}
