<?php

class Book extends Product{

    public $weight;

    public function __construct($sku, $name, $price, $type, $weight)
    {
        parent::__construct($sku, $name, $price, $type);
        $this->weight = $weight;
    }

    public function getSpecialParams(){
        return "Weight: ".$this->weight." KG";
    }

}