<?php

class DVD extends Product{

    public $size;

    public function __construct($sku, $name, $price, $type, $size)
    {
        parent::__construct($sku, $name, $price, $type);
        $this->size = $size;
    }

    public function getSpecialParams(){
        return "Size: ".$this->size." MB";
    }

}