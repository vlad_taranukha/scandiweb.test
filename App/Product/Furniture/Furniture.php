<?php

class Furniture extends Product{
    public $height;
    public $width;
    public $length;

    public function __construct($sku, $name, $price, $type, $height, $width, $length)
    {
        parent::__construct($sku, $name, $price, $type);
        $this->height = $height;
        $this->width = $width;
        $this->length = $length;
    }

    public function getSpecialParams(){
        return "Dimensions: ".$this->height."x".$this->width."x".$this->length;
    }
}