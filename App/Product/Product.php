<?php

class Product extends App {
    public $sku;
    public $name;
    public $price;
    public $type;

    public function __construct($sku, $name, $price, $type)
    {
        $this->sku = $sku;
        $this->name = $name;
        $this->price = $price;
        $this->type = $type;
    }

    public static function dbConnect()
    {
        return new mysqli(HOST, USERNAME, PASS, DBNAME);
    }

    public static function getSkuList()
    {
        $mysqli = self::dbConnect();
        $sql = "SELECT sku FROM `products`";
        $res = $mysqli->query($sql);
        $rows = [];
        while($row = $res->fetch_array(MYSQLI_ASSOC))
        {
            $rows[] = stripcslashes($row['sku']);
        }
        $mysqli->close();
        return json_encode($rows);
    }

    public static function addProduct()
    {
        $productType = $_POST['productType'];
        foreach ($_POST as $key=>$item) {
            $item = trim($item);
            if ($item == '') {
                unset($_POST[$key]);
            }
        }
        unset($item);

        $product = new $productType(...array_values($_POST));
        $mysqli = self::dbConnect();
        $product -> name = $mysqli->real_escape_string(trim($product->name));
        $product -> sku = $mysqli->real_escape_string(trim($product->sku));
        $objProps = array_values(get_object_vars($product));
        foreach ($objProps as $objPropKey => $objPropVal) {
            $objProps[$objPropKey] = "'".$objPropVal."'";
        }
        $sql = "INSERT INTO `products` ("
            . implode(', ', array_keys(get_object_vars($product)))
            . ") VALUES (" . implode(', ', $objProps) . ")";
        $mysqli->query($sql);
        $mysqli->close();
        header("Location: ".$_SERVER['REQUEST_URI']);
    }

    public static function deleteProducts()
    {
        $mysqli = self::dbConnect();
        $productsToDelete = $_POST['productsToDelete'];
        $sql = "DELETE FROM `products` WHERE `sku` IN (".$productsToDelete.")";
        $mysqli->query($sql);
        $mysqli->close();
        header("Location: ".$_SERVER['REQUEST_URI']);
    }

    public static function showProducts()
    {
        $mysqli = self::dbConnect();
        $sql = "SELECT sku, name, price, type, size, weight, height, width, length FROM `products` WHERE 1";
        $res = $mysqli->query($sql);
        if ($res->num_rows == 0) {
            ?>
            <h2 class="no-products">Nothing to show. Please, add products first.</h2>
            <?php
            return;
        }
        while ($row = $res->fetch_assoc()) {
            $new_row = array_diff($row, array('', NULL, false));
            $obj = new $new_row['type'](...array_values($new_row));
            ?>
            <div class="product">
                <input type="checkbox" class="delete-checkbox" value="<?=htmlspecialchars($obj->sku);?>">
                <div class="product-info">
                    <p class="product-id"><?=$obj->sku;?></p>
                    <p class="product-name"><?=$obj->name;?></p>
                    <p class="product-price"><span><?=$obj->price;?></span> $</p>
                    <p class="product-characteristics"><?=$obj->getSpecialParams();?></p>
                </div>
            </div>
            <?php
        }
    }
}